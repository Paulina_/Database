# Документация по проекту База данных «Зоомагазин»
База данных предназанчена для хранения и обработки информации о клиентах / сотрудниках / заказах / продуктах.

### Таблицы. Схема
Данная БД содержит 9 таблиц. Их схема и взаимосвязь:
![таблицы](/img/table_schema.png)
### Таблицы. Описание
 - таблица `products` содержит информацию о продуктах, которые продаются в зоомагазине. Поля — id, категория продукта и его имя.
Создадим таблицу:

```sql
    CREATE TABLE IF NOT EXISTS `products` (
    	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    	`category_id` int(10) NOT NULL,
    	`name` varchar(50) DEFAULT NULL,
    	PRIMARY KEY (`id`)	
    );
    
```
 - в таблице `products_categories` находятся категории продуктов.
Создадим таблицу:

```sql
    CREATE TABLE IF NOT EXISTS `products_categories` (
    	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    	`name` varchar(45) DEFAULT NULL,
    	PRIMARY KEY (`id`)	
    );
```
 - таблица `price_history` нужна для того, что бы хранить историю цен на товары. Это, например, может понадобиться в том случае, когда покупатель возвращает купленный им товар, а цена на него уже изменилась.
Создадим таблицу:

```sql
    CREATE TABLE IF NOT EXISTS `price_history` (
    	`product_id` int(10) unsigned NOT NULL,
    	`date` date NOT NULL,
    	`time` time NOT NULL,
    	`price` float(3) NOT NULL
    );
```
 - таблица `co_workers` используются для хранения информации о сотрудниках зоомагазина.
Создадим таблицу:

```sql
    CREATE TABLE IF NOT EXISTS `co-workers` (
    	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    	`firstname` varchar(45) DEFAULT NULL,
    	`lastname` varchar(45) DEFAULT NULL,
    	`dbirth` date DEFAULT NULL,
    	`email` varchar(30) DEFAULT NULL,
    	`phone` char(12) DEFAULT NULL,
    	`address` varchar(80) DEFAULT NULL,
    	`gender` enum('M','F') DEFAULT NULL,
    	PRIMARY KEY (`id`)
    );
```
 - таблица `positions` создана для того, чтобы хранить должности/профессии работников. Поля в этой таблице — id и название профессии
Создадим таблицу:

```sql
    CREATE TABLE IF NOT EXISTS `positions` (
    	`id` int(3) unsigned NOT NULL AUTO_INCREMENT,
    	`name` varchar(50) DEFAULT NULL,
    	PRIMARY KEY (`id`)
    );
```
 - таблица `appointment_to_position` - назначение на должность. Эта таблица была создана для того, чтобы можно было добавить одному работнику несколько должностей.
Создадим таблицу:

```sql
    CREATE TABLE IF NOT EXISTS `appointment_to_position` (
    	`co_worker_id` int(10) unsigned NOT NULL,
    	`position_id` int(10) unsigned NOT NULL
    );
```
 - таблица `clients` содержит информацию о покупателях, т.е. id, фамилия, имя и номер телефона
Создадим таблицу:

```sql
    CREATE TABLE IF NOT EXISTS `clients` (
    	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    	`firstname` varchar(45) DEFAULT NULL,
    	`lastname` varchar(45) DEFAULT NULL,
    	`phone` char(12) DEFAULT NULL,
    	`gender` enum('M','F') DEFAULT NULL,
    	PRIMARY KEY (`id`)
    );
```
 - таблица `orders` предназанчена для формирования заказа. Она содержит такие поля как id заказа, id клиента, id работника, который оформлял этот заказ, дату и время оформления заказа, сумма заказа.
Создадим таблицу:

 ```sql
    CREATE TABLE IF NOT EXISTS `orders` (
    	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    	`client_id` int(10) unsigned NOT NULL,
    	`co-worker_id` int(10) unsigned NOT NULL,
    	`date` date DEFAULT NULL, 
    	`time` time DEFAULT NULL,
    	`order_sum` float(7) NOT NULL,
    	PRIMARY KEY (`id`)	
    );
```
 - таблица `orders_table` создана для того, чтобы хранить информацию о заказе. Поля — id заказа, id клиента и количество товара. Без этой таблицы не было бы возможности покупать несколько видов продуктов за один заказ.
Создадим таблицу:

 ```sql
    CREATE TABLE IF NOT EXISTS `orders_table` (
	`order_id` int(10) unsigned NOT NULL,
	`product_id` int(10) unsigned NOT NULL,
	`product_count` int(7) unsigned NOT NULL
    );
```
### Описание связей между таблицами.
##### Связь многие ко многим:
 - `co_workers` - `positions` (один сотрудник может работать на нескольких должностях и на каждую должность может приходиться несколько сотрудников). Для этой связи была создана дополнительная таблица  `appointment_to_position` - назначение на должность.
 - `orders` - `products` (в каждом заказе может быть несколько продуктов, каждый продукт может быть во многих заказах). Для этих двух таблиц была создана дополнительная таблица `orders_table`. В ней хранится id продукта и id конкретного заказа.
##### Связь один ко многим:
 - `orders` — `clients`  (у каждого клиента может быть несколько заказов, но у одного заказа есть только один заказчик).
`products_categories` - `products`  (у каждого товара может быть только одна категория,  у каждой категории может быть много товаров).
 - `products` - `price_history` (у каждого продукта может быть несколько цен).

### Типовые операции
##### Вставка данных
 - Составим заказ для нового клиента.
 
 ```sql
 INSERT INTO `clients` (`firstname`, `lastname`, `phone`, `gender`) VALUES
	('Kelly', 'Sousa', '44567895', 'F');


INSERT INTO `orders` (`client_id`, `co-worker_id`, `date`, `time`, `order_sum`) VALUES (
	(
		SELECT id FROM `clients` WHERE `phone` = '44567895' AND `lastname` = 'Sousa'),
		5, 
		CURDATE(),
		CURTIME(), 
		(SELECT `price` FROM `price_history`
		WHERE `price_history`.`product_id` = (SELECT `id` FROM `products` WHERE `name` = 'parrot')
		AND `price_history`.`date`= (SELECT MAX(`date`) FROM `price_history` WHERE `date` <= (CURDATE()) AND `product_id` = (SELECT `id` FROM `products` WHERE `name` = 'parrot')))
	);

INSERT INTO `orders_table` VALUES
	(
		(SELECT MAX(orders.id) FROM `orders` LIMIT 1),
		(SELECT `id` FROM `products` WHERE `name` = 'parrot'),
		1
	);
	
 ```

##### Обновление данных
 
  - Обновим номер телефона сотрудника.
 
```sql
    UPDATE `co-workers` SET `phone` = '41234525' WHERE `lastname` = 'Miina' AND phone = '42680356' LIMIT 1;
```

##### Удаление данных

 - Удалим сотрудника

```sql
    DELETE FROM `co-workers` WHERE `id` = 9  LIMIT 1;
    DELETE FROM `appointment_to_position` WHERE id = `9` LIMIT 5;
```

### Типовые запросы

 - Найти самый популярный товар.
```sql
    SELECT t1.name as t, MAX(t1.amount) as tt
    FROM (
    	SELECT p.name name, SUM(ot.product_count) amount
    	FROM `products` p
    	INNER JOIN `orders_table` ot ON ot.product_id = p.id 
    	GROUP BY ot.product_id) as t1
    GROUP BY t
    ORDER BY t DESC
    LIMIT 1
```

 - Посмотрим, сколько стоил продукт на момент его продажи (например, для того, чтобы узнать точную сумму при возврате товара).

```sql
    SELECT `price` FROM `price_history`
    WHERE `price_history`.`product_id` = 15 
    AND  `price_history`.`date`= (SELECT MAX(`date`) FROM `price_history` WHERE `date` <= (SELECT `date` FROM `orders` WHERE `orders`.id = 16))
    LIMIT 100;
```

 
### Представления

 - Узнаем какое количество продуктов было продано за всё время

```sql
    CREATE VIEW `sold_products` AS
    SELECT p.name, SUM(ot.product_count) amount
    FROM `products` p
    INNER JOIN `orders_table` ot ON ot.product_id = p.id 
    GROUP BY ot.product_id,
    LIMIT 100;
```

 - Найдем клиента, который купил на самую большу сумму (например, что бы дать ему скидку)

```sql
    CREATE VIEW `good_client` AS
    SELECT c.id, c.firstname, c.lastname, SUM(o.order_sum) as all_sum
    FROM `clients`  c, `orders` o
    WHERE c.id= o.client_id
    GROUP BY (c.firstname)
    ORDER BY SUM(o.order_sum) DESC
    LIMIT 1
```